const num1 = getNumber('num1');
const num2 = getNumber('num2');
const type = prompt('Введите одну из математических операций + - * /', '')

let convertType = (num1 = 4, num2 = 6, type = '+') => { // using default parameters
    switch (type) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;
        default:
            alert('There is no operation availbale!');
    }
}

console.log(convertType(num1, num2, type)); 

function getNumber(title) { 
   do {
     num = prompt(`Введите число ${title}`);
    }  while (isNaN(num) || num === '');
    
    return Number(num);
}