const n = checkNumber('порядковый номер');
const f0 = checkNumber('первое число');
const f1 = checkNumber('второе число');

function checkNumber(title) {
    let num;

    do {
        num = +prompt(`Input ${title}`)
    } while (num !== null && !Number.isInteger(num))

    return num;
}

function fib(f0, f1, n) {
    switch (true) {
        case n == 0:
            return f0;
        case (n > 0 && n <= 2):
            return f1;
        case n == -1:
            return -f1;
        case n == -2:
            return f1;
        case n < 0:
            return fib(f0, f1, n + 2) - fib(f0, f1, n + 1);
        default:
            return fib(f0, f1, n - 1) + fib(f0, f1, n - 2);
    }
}

alert(`${n}-ое число: ${fib(f0, f1, n)}`);

/*
 
-5 = 10
-4 = -6
-3 = 4
-2 = -2
-1 = 2
0 = 1
*/
